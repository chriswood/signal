﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Signal.Compiler {
	public interface IEquationCompiler {
		Delegate Compile(string equation);

		IEnumerable<ParameterExpression> Variables { get; }
	}
}