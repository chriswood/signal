﻿using Ninject.Modules;
using Signal.Compiler.Lexing;
using Signal.Compiler.Parsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Compiler {
	public class CompilerNinjectModule : NinjectModule {
		public override void Load() {
			Bind<ILexerStateMachine>().To<LexerStateMachine>();
			Bind<ILexer>().To<Lexer>();

			Bind<IParser>().To<Parser>();

			Bind<IEquationCompiler>().To<EquationCompiler>();
		}
	}
}
