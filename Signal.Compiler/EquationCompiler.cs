﻿using Signal.Compiler.Lexing;
using Signal.Compiler.Parsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Compiler {
	internal class EquationCompiler : IEquationCompiler {
		private readonly ILexer _lexer;
		private readonly IParser _parser;

		public EquationCompiler(ILexer lexer, IParser parser) {
			_lexer = lexer;
			_parser = parser;
		}

		public Delegate Compile(string equation) {
			// Use the lexer to convert the string into tokens
			var tokens = _lexer.Tokenize(equation);

			// Parse the tokens into an expression tree
			var expression = _parser.Parse(tokens);

			// Compile the expression tree and return the resulting delegate
			return Expression.Lambda(expression, _parser.Variables).Compile();
		}

		public IEnumerable<ParameterExpression> Variables {
			get {
				return _parser.Variables;
			}
		}
	}
}
