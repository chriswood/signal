﻿using Signal.Compiler.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Compiler.Lexing {
	internal class StateConstant : LexerState {
		private readonly StringBuilder _constant = new StringBuilder();

		public StateConstant(ILexerStateMachine lexer, int startPosition) : base(lexer, startPosition) { }

		private void OutputToken() {
			float constantValue;

			if (!float.TryParse(_constant.ToString(), out constantValue)) {
				// This is not a valid float
				throw new Exception($"Invalid constant value {_constant.ToString()} at position {StartPosition}");
			}

			// Output the token that we have produced
			Lexer.AddToken(new ConstantToken(StartPosition, constantValue));
		}

		public override bool ProcessChar(int position, char c) {
			if (char.IsDigit(c)) {
				// Add the character to the constant value
				_constant.Append(c);

				// Move onto the next character
				return true;
			} else if (c == '.') {
				if (_constant.ToString().Contains('.')) {
					// We already have a decimal so this is invalid
					throw new Exception($"Unexpected decimal at position {position}");
				}

				// Add the decimal character to the constant value
				_constant.Append(c);

				// Move onto the next character
				return true;
			} else if (Operations.Contains(c)) {
				// Process and output the token that we have built
				OutputToken();

				// Transition to StateAll
				Lexer.CurrentState = new StateAll(Lexer, position);

				// Don't move onto the next character
				return false;
			} else if (char.IsLetter(c)) {
				// Process and output the token that we have built
				OutputToken();

				// Transition to StateSeries
				Lexer.CurrentState = new StateSeries(Lexer, position);

				// Don't move onto the next character
				return false;
			} else if(c == CharConstants.EndOfTransmission) {
				// If _constant ends with a . then we don't have a full valid token
				if (_constant.ToString().EndsWith(".")) {
					throw new Exception("Unexpected end of expression");
				}

				// There will be no more input, output what we have got
				OutputToken();

				return true;
			} else {
				throw new Exception($"Unexpected character '{c}' at position {position}");
			}
		}
	}
}
