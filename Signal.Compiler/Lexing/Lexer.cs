﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Signal.Compiler.Tokens;

namespace Signal.Compiler.Lexing {
	internal class Lexer : ILexer {
		private readonly ILexerStateMachine _stateMachine;

		public Lexer(ILexerStateMachine stateMachine) {
			_stateMachine = stateMachine;
		}

		public IEnumerable<Token> Tokenize(string equation) {
			// Clear anything left from previous calls
			_stateMachine.Initialize();

			int position = 0;
			for (; position < equation.Length;) {
				char c = equation[position];

				// Ignore any whitespace
				if(char.IsWhiteSpace(c)) {
					position++;
					continue;
				}

				bool advance = _stateMachine.CurrentState.ProcessChar(position, c);

				if (advance) {
					position++;
				}
			}

			// All of the input string has been processed. Send the EndOfTransmission charater to capture
			// the content of the remaining state.
			_stateMachine.CurrentState.ProcessChar(position, CharConstants.EndOfTransmission);

			return _stateMachine.Tokens;
		}
	}
}
