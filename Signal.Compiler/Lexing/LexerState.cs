﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Compiler.Lexing {
	// TODO Handle negative numbers. - always a negative symbol not subtract? Does that even matter?
	internal abstract class LexerState {
		private readonly ILexerStateMachine _lexer;
		private readonly int _startPosition;

		protected char[] Operations = new char[] { '+', '-', '*', '/', '(', ')' };
		
		public LexerState(ILexerStateMachine lexer, int startPosition) {
			_lexer = lexer;
			_startPosition = startPosition;
		}
		
		public abstract bool ProcessChar(int position, char c);

		protected ILexerStateMachine Lexer {
			get {
				return _lexer;
			}
		}

		protected int StartPosition {
			get {
				return _startPosition;
			}
		}
	}
}
