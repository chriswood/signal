﻿using Signal.Compiler.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Compiler.Lexing {
	internal class StateSeries : LexerState {
		private readonly StringBuilder _series = new StringBuilder();

		public StateSeries(ILexerStateMachine lexer, int startPosition) : base(lexer, startPosition) { }

		public override bool ProcessChar(int position, char c) {
			if (char.IsLetter(c)) {
				// Add the character to the series name
				_series.Append(c);

				// Move onto the next character
				return true;
			} else if (Operations.Contains(c) || c == CharConstants.EndOfTransmission) {
				// Output the token that we have produced
				Lexer.AddToken(new SeriesToken(StartPosition, _series.ToString()));

				// Transition to StateAll
				Lexer.CurrentState = new StateAll(Lexer, position);

				// Don't move onto the next character
				return false;
			} else {
				throw new Exception($"Unexpected character '{c}' at position {position}");
			}
		}
	}
}
