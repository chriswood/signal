﻿using Signal.Compiler.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Compiler.Lexing {
	public interface ILexer {
		IEnumerable<Token> Tokenize(string equation);
	}
}
