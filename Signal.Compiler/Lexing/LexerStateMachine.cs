﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Signal.Compiler.Tokens;

namespace Signal.Compiler.Lexing {
	internal class LexerStateMachine : ILexerStateMachine {
		private readonly List<Token> _tokens = new List<Token>();

		public LexerState CurrentState {
			get;
			set;
		}

		public void AddToken(Token token) {
			_tokens.Add(token);
		}

		public IEnumerable<Token> Tokens {
			get {
				return _tokens;
			}
		}

		public void Initialize() {
			_tokens.Clear();
			CurrentState = new StateAll(this, 0);
		}
	}
}
