﻿using Signal.Compiler.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Compiler.Lexing {
	internal class StateAll : LexerState {
		public StateAll(ILexerStateMachine lexer, int startPosition) : base(lexer, startPosition) { }

		public override bool ProcessChar(int position, char c) {
			if (char.IsLetter(c)) {
				// This is the begining of a series
				Lexer.CurrentState = new StateSeries(Lexer, position);

				// Don't move onto the next character
				return false;
			} else if (char.IsDigit(c)) {
				// This is the begining of a constant
				Lexer.CurrentState = new StateConstant(Lexer, position);

				// Don't move onto the next character
				return false;
			} else if (c == '*') {
				// Only a single character for this token so no need for another state
				Lexer.AddToken(new MultiplicationToken(StartPosition));

				// Move onto the next character
				return true;
			} else if (c == '/') {
				// Only a single character for this token so no need for another state
				Lexer.AddToken(new DivisionToken(StartPosition));

				// Move onto the next character
				return true;
			} else if (c == '+') {
				// Only a single character for this token so no need for another state
				Lexer.AddToken(new AdditionToken(StartPosition));

				// Move onto the next character
				return true;
			} else if (c == '-') {
				// Only a single character for this token so no need for another state
				Lexer.AddToken(new SubtractionToken(StartPosition));

				// Move onto the next character
				return true;
			} else if (c == '(') {
				// Only a single character for this token so no need for another state
				Lexer.AddToken(new OpenBraceToken(StartPosition));

				// Move onto the next character
				return true;
			} else if (c == ')') {
				// Only a single character for this token so no need for another state
				Lexer.AddToken(new CloseBraceToken(StartPosition));

				// Move onto the next character
				return true;
			} else if(c == CharConstants.EndOfTransmission) {
				// Nothing to do
				return true;
			} else {
				throw new Exception($"Unexpected character '{c}' at position {position}");
			}
		}
	}
}
