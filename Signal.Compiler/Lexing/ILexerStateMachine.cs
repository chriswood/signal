﻿using Signal.Compiler.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Compiler.Lexing {
	/// <summary>
	/// A representation of a state machine used during lexing operations.
	/// </summary>
	internal interface ILexerStateMachine {
		/// <summary>
		/// Initialise the state machine ready to being lexing. This will clear the result of any previous lexing operation.
		/// </summary>
		void Initialize();
		
		/// <summary>
		/// The current state of this state machine. This is used both to modify the contents of the current state and by the LexerStates to transition from one state to another.
		/// </summary>
		LexerState CurrentState { get; set; }

		/// <summary>
		/// Record a token that has been processed.
		/// </summary>
		/// <param name="token"></param>
		void AddToken(Token token);

		/// <summary>
		/// A list of all of the tokens that have been processed since the last call to Initialize.
		/// </summary>
		IEnumerable<Token> Tokens { get; }
	}
}
