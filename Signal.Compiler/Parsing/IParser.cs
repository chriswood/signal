﻿using Signal.Compiler.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Compiler.Parsing {
	public interface IParser {
		/// <summary>
		/// Parse the tokens into an delegate that can be called with the appropriate parameters to evaluate the expression.
		/// </summary>
		/// <param name="tokens"></param>
		/// <returns></returns>
		Expression Parse(IEnumerable<Token> tokens);

		/// <summary>
		/// The variables that are processed during the parse. Each one of these will need to be supplied to the delegate returned from the Parse method.
		/// </summary>
		IEnumerable<ParameterExpression> Variables { get; }
	}
}
