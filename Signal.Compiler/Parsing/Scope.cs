﻿using Signal.Compiler.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Compiler.Parsing {
	internal class Scope {
		private readonly Scope _parent;

		public Scope(Scope parent) {
			_parent = parent;
		}

		public Expression Expression { get; set; }

		public Token Operation { get; set; }

		public bool ExpressionIsComplete {
			get {
				// No expression in scope
				if (Expression == null) return false;

				// Invalid expression, nothing following operation
				if (Operation != null) return false;

				// We have a valid expression
				return true;
			}
		}
	}
}
