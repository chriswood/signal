﻿using Signal.Compiler.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Compiler.Parsing {
	internal class Parser : IParser {
		private readonly List<ParameterExpression> _variables = new List<ParameterExpression>();

		private readonly Stack<Scope> _scope = new Stack<Scope>();

		private void Initialize() {
			_scope.Clear();
			_variables.Clear();
		}

		public Expression Parse(IEnumerable<Token> tokens) {
			Initialize();

			if (tokens == null || !tokens.Any()) {
				throw new ArgumentException(nameof(tokens));
			}

			// Create a queue of tokens that we have yet to process
			var remainingTokens = new Queue<Token>(tokens);

			// Create the initial root scope by surrounding the entire expression with braces
			OpenScope();

			// Process the tokens
			while (remainingTokens.Any()) {
				var token = remainingTokens.Dequeue();

				ProcessToken(token);
			}

			if (_scope.Count != 1) {
				throw new Exception("More open braces than close braces");
			}

			if (!CurrentScope.ExpressionIsComplete) {
				throw new Exception("Invalid end of expression");
			}

			return CurrentScope.Expression;
		}

		private void ProcessToken(Token token) {
			if (token is AdditionToken || token is MultiplicationToken || token is SubtractionToken || token is DivisionToken) {
				if (CurrentScope.Operation != null) {
					throw new Exception("Unexpected operation at " + token.StartPosition);
				}
				CurrentScope.Operation = token;
			} else if (token is ConstantToken) {
				if (CurrentScope.Expression != null && CurrentScope.Operation == null) {
					throw new Exception("Unexpected constant at " + token.StartPosition);
				}

				// Create the constant expression from the token and add it to the current state
				var constant = Expression.Constant(((ConstantToken)token).Value);

				AddToCurrentState(constant);
			} else if (token is SeriesToken) {
				if (CurrentScope.Expression != null && CurrentScope.Operation == null) {
					// Implicit multiplication
					CurrentScope.Operation = new MultiplicationToken(token.StartPosition);
				}

				ParameterExpression variable;

				// See if we have already created a parameter for this variable
				variable = Variables.FirstOrDefault(s => s.Name == token.Name);

				if (variable == null) {
					// We need to create a new one
					variable = Expression.Variable(typeof(float), token.Name);

					// Add the variable to the list of those required when the compiled delegate is invoked
					AddVariable(variable);
				}

				// Add the expression to the current state
				AddToCurrentState(variable);
			} else if (token is CloseBraceToken) {
				if (_scope.Count == 1) {
					// The root scope cannot be closed
					throw new Exception("Unmatched close brace at " + token.StartPosition);
				}

				// Check that we are ready for a close brace
				if (!CurrentScope.ExpressionIsComplete) {
					throw new Exception("Unexpected close brace at " + token.StartPosition);
				}

				// Close the scope
				var closedScope = CloseScope();

				// Get the expression that was defined within the scope that has just been closed
				var expression = closedScope.Expression;

				// Incorporate the expression
				AddToCurrentState(expression);
			} else if (token is OpenBraceToken) {
				// Handle implicit multiplication
				if (CurrentScope.Expression != null && CurrentScope.Operation == null) {
					CurrentScope.Operation = new MultiplicationToken(token.StartPosition);
				}

				// Create a new scope
				OpenScope();
			} else {
				throw new InvalidOperationException("Unexpected token at " + token.StartPosition);
			}
		}

		private void AddToCurrentState(Expression tokenExpression) {
			if (CurrentScope.Expression == null) {
				// If we don't already have the LHS then this is it
				CurrentScope.Expression = tokenExpression;
			} else {
				// This is a valid token for the RHS of the expression
				Expression operationExpression;

				if (CurrentScope.Operation is AdditionToken) {
					operationExpression = Expression.Add(CurrentScope.Expression, tokenExpression);
				} else if (CurrentScope.Operation is SubtractionToken) {
					operationExpression = Expression.Subtract(CurrentScope.Expression, tokenExpression);
				} else if (CurrentScope.Operation is MultiplicationToken) {
					operationExpression = Expression.Multiply(CurrentScope.Expression, tokenExpression);
				} else if (CurrentScope.Operation is DivisionToken) {
					operationExpression = Expression.Divide(CurrentScope.Expression, tokenExpression);
				} else throw new NotImplementedException("Unhandled token");

				CurrentScope.Expression = operationExpression;
				CurrentScope.Operation = null;
			}
		}

		private void OpenScope() {
			var scope = new Scope(CurrentScope);

			_scope.Push(scope);
		}

		private Scope CloseScope() {
			return _scope.Pop();
		}

		private Scope CurrentScope {
			get {
				return _scope.Any() ? _scope.Peek() : null;
			}
		}

		public IEnumerable<ParameterExpression> Variables {
			get {
				return _variables;
			}
		}

		private void AddVariable(ParameterExpression series) {
			if (!_variables.Any(s => s.Name == series.Name)) {
				_variables.Add(series);
			}
		}
	}
}
