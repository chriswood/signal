﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Compiler.Tokens {
	public class SeriesToken : Token {
		private string _variable;

		public SeriesToken(int startPosition, string variable) : base(startPosition) {
			_variable = variable;
		}

		public override string Name {
			get {
				return _variable;
			}
		}

		public string Variable {
			get {
				return _variable;
			}
		}
	}
}
