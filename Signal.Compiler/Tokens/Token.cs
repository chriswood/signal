﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Compiler.Tokens {
	public abstract class Token {
		private readonly int _startPosition;

		public Token(int startPosition) {
			_startPosition = startPosition;
		}

		public int StartPosition {
			get {
				return _startPosition;
			}
		}

		// TODO Consider renaming this or changing to overriding ToString instead
		public abstract string Name { get; }
	}
}
