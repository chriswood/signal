﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Compiler.Tokens {
	public class ConstantToken : Token {
		private float _value;

		public ConstantToken(int startPosition, float value) : base(startPosition) {
			_value = value;
		}

		public override string Name {
			get {
				return _value.ToString();
			}
		}

		public float Value {
			get {
				return _value;
			}
		}
	}
}
