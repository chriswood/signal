﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Compiler.Tokens {
	public class DivisionToken : Token {
		public DivisionToken(int startPosition) : base(startPosition) { }

		public override string Name {
			get {
				return "/";
			}
		}
	}
}
