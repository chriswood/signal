﻿using System.Windows;

namespace Signal.Ui.Services {
	public interface IDialogService {
		string OpenFileDialog(string filter);
		MessageBoxResult ShowMessageBox(string messageBoxText, string caption, MessageBoxButton button, MessageBoxImage image);
	}
}