﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Signal.Ui.Services {
	public class DialogService : IDialogService {
		public string OpenFileDialog(string filter) {
			var dialog = new OpenFileDialog() {
				Filter = filter,
				Multiselect = false
			};

			if(dialog.ShowDialog() == true) {
				return dialog.FileName;
			} else {
				return null;
			}
		}

		public MessageBoxResult ShowMessageBox(string messageBoxText, string caption, MessageBoxButton button, MessageBoxImage image) {
			return MessageBox.Show(messageBoxText, caption, button, image);
		}
	}
}
