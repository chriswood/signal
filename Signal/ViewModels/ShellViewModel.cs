﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Ui.ViewModels {
	public class ShellViewModel {
		private readonly SeriesListViewModel _seriesListViewModel;

		public ShellViewModel(SeriesListViewModel seriesListViewModel) {
			_seriesListViewModel = seriesListViewModel;
		}

		public SeriesListViewModel SeriesListViewModel {
			get {
				return _seriesListViewModel;
			}
		}
	}
}
