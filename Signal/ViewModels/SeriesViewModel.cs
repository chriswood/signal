﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Signal.Core.Timeseries;
using System.Collections.ObjectModel;
using System.Windows.Media;

namespace Signal.Ui.ViewModels {
	public class SeriesViewModel : PropertyChangedBase {
		// Deliberate use of American spelling for consistency with the API
		private static readonly Color[] _colors;

		private static int _instanceNumber = 0;

		static SeriesViewModel() {
			_colors = new Color[] {
				(Color)ColorConverter.ConvertFromString("#a6cee3"),
				(Color)ColorConverter.ConvertFromString("#1f78b4"),
				(Color)ColorConverter.ConvertFromString("#b2df8a"),
				(Color)ColorConverter.ConvertFromString("#33a02c"),
				(Color)ColorConverter.ConvertFromString("#fb9a99"),
				(Color)ColorConverter.ConvertFromString("#e31a1c"),
				(Color)ColorConverter.ConvertFromString("#fdbf6f"),
				(Color)ColorConverter.ConvertFromString("#ff7f00"),
				(Color)ColorConverter.ConvertFromString("#cab2d6")
			};
		}

		public SeriesViewModel() {
			Color = _colors[_instanceNumber % _colors.Length];

			_instanceNumber++;
		}

		private string _Name;
		public string Name {
			get { return _Name; }
			set {
				if (_Name != value) {
					_Name = value;
					NotifyOfPropertyChange(() => Name);
				}
			}
		}

		private bool _IsSelected;
		/// <summary>
		/// Whether or not this series is currently selected in the list.
		/// </summary>
		public bool IsSelected {
			get { return _IsSelected; }
			set {
				if (_IsSelected != value) {
					_IsSelected = value;
					NotifyOfPropertyChange(() => IsSelected);
				}
			}
		}

		private bool _IsDisplayed;
		/// <summary>
		/// Whether or not this series is currently being displayed in the chart.
		/// </summary>
		public bool IsDisplayed {
			get { return _IsDisplayed; }
			set {
				if (_IsDisplayed != value) {
					_IsDisplayed = value;
					NotifyOfPropertyChange(() => IsDisplayed);
				}
			}
		}

		public ReadOnlyCollection<Datapoint> Values { get; internal set; }

		public Color Color { get; private set; }

		/// <summary>
		/// The parameter name that is used to reference this series in an equation.
		/// </summary>
		public char Parameter { get; set; }

		/// <summary>
		/// A property that can be used for storing data for the view. This enables us to show/hide the series without having to process
		/// the data into the library specific types required by the view each time.
		/// </summary>
		public object Tag { get; set; }
	}
}
