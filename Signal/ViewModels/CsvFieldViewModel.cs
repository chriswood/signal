﻿
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Ui.ViewModels {
	public class CsvFieldViewModel : PropertyChangedBase {
		private string _FieldName;
		public string FieldName {
			get { return _FieldName; }
			set {
				if (_FieldName != value) {
					_FieldName = value;
					NotifyOfPropertyChange(() => FieldName);
				}
			}
		}

		private bool _IsSelected;
		public bool IsSelected {
			get { return _IsSelected; }
			set {
				if (_IsSelected != value) {
					_IsSelected = value;
					NotifyOfPropertyChange(() => IsSelected);
				}
			}
		}
	}
}
