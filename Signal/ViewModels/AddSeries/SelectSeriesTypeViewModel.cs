﻿using Caliburn.Micro;

using Signal.Ui.ViewModels.Wizard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Ui.ViewModels.AddSeries {
	public class SelectSeriesTypeViewModel : WizardPageBase {
		private readonly CsvSelectFileViewModel _addSeriesCsvSelectFileVm;
		private readonly EnterEquationViewModel _enterEquationViewModel;

		public SelectSeriesTypeViewModel(CsvSelectFileViewModel addSeriesCsvSelectFileVm, EnterEquationViewModel enterEquationViewModel) {
			_addSeriesCsvSelectFileVm = addSeriesCsvSelectFileVm;
			_enterEquationViewModel = enterEquationViewModel;
		}

		public void AddFromCsv() {
			_addSeriesCsvSelectFileVm.Activate(Parent);
		}

		public void AddFromEquation() {
			_enterEquationViewModel.Activate(Parent);
		}
	}
}
