﻿using Caliburn.Micro;

using Signal.Ui.Services;
using Signal.Ui.ViewModels.Wizard;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Signal.Ui.ViewModels.AddSeries {
	public class CsvSelectFileViewModel : WizardPageBase {
		private readonly IDialogService _dialogService;
		private readonly CsvSelectTimeFieldViewModel _addSeriesCsvSelectFieldsVm;

		public CsvSelectFileViewModel(IDialogService dialogService, CsvSelectTimeFieldViewModel addSeriesCsvSelectFieldsVm) {
			_dialogService = dialogService;
			_addSeriesCsvSelectFieldsVm = addSeriesCsvSelectFieldsVm;
		}

		private string _CsvPath;
		public string CsvPath {
			get { return _CsvPath; }
			set {
				if (_CsvPath != value) {
					_CsvPath = value;
					NotifyOfPropertyChange(() => CsvPath);

					NotifyOfPropertyChange(() => CanAdvance);
				}
			}
		}

		#region Browse Command

		public void Browse() {
			var selectedPath = _dialogService.OpenFileDialog("Comma Separated Value Files (*.csv)|*.csv|All File s(*.*)|*.*");

			if (!string.IsNullOrEmpty(selectedPath)) {
				CsvPath = selectedPath;
			}
		}

		#endregion

		#region Advance Command

		public bool CanAdvance {
			get {
				return File.Exists(CsvPath);
			}
		}

		public void Advance() {
			try {
				_addSeriesCsvSelectFieldsVm.LoadPossibleTimeFields(CsvPath);

				_addSeriesCsvSelectFieldsVm.Activate(Parent);
			} catch (Exception ex) {
				_dialogService.ShowMessageBox("An error occurred whilst reading the CSV file: " + ex.Message, "Error Reading CSV", MessageBoxButton.OK, MessageBoxImage.Error);
			}

		}

		#endregion
	}
}
