﻿using Caliburn.Micro;
using Signal.Ui.Extensions;
using Signal.Ui.ViewModels.Wizard;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using Signal.Core;
using Signal.Ui.Services;
using System.Windows;
using System.Diagnostics.Contracts;
using Signal.Ui.Events;
using Signal.Core.Timeseries;

namespace Signal.Ui.ViewModels.AddSeries {
	public class CsvSelectValueFieldsViewModel : WizardPageBase {
		private readonly ICsvImporter _csvImporter;
		private readonly IDialogService _dialogService;
		private readonly IEventAggregator _eventAggregator;
		private readonly ISeriesPool _seriesPool;

		private string _path = null;
		private string _timeField = null;

		public CsvSelectValueFieldsViewModel(ICsvImporter csvImporter, IDialogService dialogService, IEventAggregator eventAggregator, ISeriesPool seriesPool) {
			_csvImporter = csvImporter;
			_dialogService = dialogService;
			_eventAggregator = eventAggregator;
			_seriesPool = seriesPool;

			Fields.AddChildPropertyChangedHandler(
				(s,e) => NotifyOfPropertyChange(() => CanAdvance)
			);
		}

		/// <summary>
		/// Load a list of fields from the CSV that may contain the values
		/// </summary>
		/// <param name="path">The path the the CSV file</param>
		/// <param name="timeField">The field that contains the time values</param>
		public void LoadSelectableValueFields(string path, string timeField) {
			// Save the path to the CSV and the field containing the time for when we actually import the data
			_path = path;
			_timeField = timeField;

			// Clear any existing fields
			Fields.Clear();

			// Create a new CsvFieldViewModel for each field in the CSV file and add it to Fields
			try {
				foreach(var field in _csvImporter.ReadFields(path)) {
					// The time field cannot be selected as a value field
					if (field == timeField) continue;

					Fields.Add(
						new CsvFieldViewModel() {
							FieldName = field
						}
					);
				}
			} catch(Exception ex) {
				_dialogService.ShowMessageBox("An error occurred whilst importing the CSV data: " + ex.Message, "Error Reading CSV", MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

		public ObservableCollection<CsvFieldViewModel> Fields { get; private set; } = new ObservableCollection<CsvFieldViewModel>();

		public IEnumerable<CsvFieldViewModel> SelectedFields {
			get {
				return Fields.Where(s => s.IsSelected);
			}
		}

		#region Advance Command

		public bool CanAdvance {
			get {
				return SelectedFields.Any();
			}
		}

		public void Advance() {
			Contract.Assert(!string.IsNullOrEmpty(_path) && !string.IsNullOrEmpty(_timeField), "LoadCsv must be called before any data can be imported");

			try {
				// Read the data for the selected fields from the CSV
				var data = _csvImporter.ReadData(_path, _timeField, from x in SelectedFields select x.FieldName);

				// For each field create a new SeriesViewModel
				foreach(var field in data.Keys) {
					var series = new Series() {
						Name = field,
						Values = data[field]
					};

					_seriesPool.AddSeries(series);
				}

				// Publist the event that enables us to update the chart and the series list
				_eventAggregator.PublishOnUIThread(new SeriesPoolChangedEvent());

				// Close the wizard
				Parent.Close();
			} catch (Exception ex) {
				string message;

				if (ex.InnerException != null) message = ex.InnerException.Message;
				else message = ex.Message;

				_dialogService.ShowMessageBox("An error occurred whilst importing the CSV data: " + message, "Error Reading CSV", MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

		#endregion
	}
}
