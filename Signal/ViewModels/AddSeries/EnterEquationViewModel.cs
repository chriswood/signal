﻿using Caliburn.Micro;
using Signal.Compiler;
using Signal.Core.Timeseries;
using Signal.Ui.Events;
using Signal.Ui.ViewModels.Wizard;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Ui.ViewModels.AddSeries {
	public class EnterEquationViewModel : WizardPageBase {
		private readonly IEquationCompiler _compiler;
		private readonly ISeriesPool _seriesPool;
		private readonly IEventAggregator _eventAggregator;

		public EnterEquationViewModel(IEquationCompiler compiler, ISeriesPool seriesPool, IEventAggregator eventAggregator) {
			_compiler = compiler;
			_seriesPool = seriesPool;
			_eventAggregator = eventAggregator;
		}

		private string _Equation;
		public string Equation {
			get { return _Equation; }
			set {
				if (_Equation != value) {
					_Equation = value;
					NotifyOfPropertyChange(() => Equation);
					NotifyOfPropertyChange(() => CanAdvance);
				}
			}
		}

		private string _SeriesName;
		public string SeriesName {
			get { return _SeriesName; }
			set {
				if (_SeriesName != value) {
					_SeriesName = value;
					NotifyOfPropertyChange(() => SeriesName);
					NotifyOfPropertyChange(() => CanAdvance);
				}
			}
		}

		private string _Error;
		public string Error {
			get { return _Error; }
			set {
				if (_Error != value) {
					_Error = value;
					NotifyOfPropertyChange(() => Error);
				}
			}
		}

		public bool CanAdvance {
			get {
				return !string.IsNullOrEmpty(Equation) && !string.IsNullOrEmpty(SeriesName);
			}
		}

		public void Advance() {
			try {
				// Attempt to compile the new equation
				var equationMethod = _compiler.Compile(Equation);

				var parameters = new List<Series>();

				foreach (var variable in _compiler.Variables) {
					// Only consider the first character of the parameter name for now
					var seriesVariable = _seriesPool.AllSeries.FirstOrDefault(s => s.Parameter == variable.Name[0]);

					if (seriesVariable == null) {
						throw new Exception($"Series with parameter value '{variable.Name}' was not found.");
					}

					parameters.Add(seriesVariable);
				}

				// We now have the series in the same order as the expected parameters to the delegate
				// Use the time values from the first series in the list as the basis for our new series

				var timeBasisSeries = _seriesPool.AllSeries.First();

				// The datapoints that wea re calculating with this equation
				var datapoints = new List<Datapoint>();

				for (int i = 0; i < timeBasisSeries.Values.Count; i++) {
					var timeBasisDatapoint = timeBasisSeries.Values[i];

					// The actual value of each of the series parameters at this point in time
					var methodParams = new List<float>();

					foreach (var seriesParameter in parameters) {
						methodParams.Add(seriesParameter.Values[i].Value);
					}

					float time = timeBasisDatapoint.Time;

					// Pass all of the parameter values to the compiled equation
					float value = (float)equationMethod.DynamicInvoke(methodParams.Cast<object>().ToArray());

					// Add the resulting datapoint to the new series
					datapoints.Add(new Datapoint(time, value));
				}

				// Add the new series to the pool
				_seriesPool.AddSeries(new Series() {
					Name = SeriesName,
					Values = datapoints.AsReadOnly()
				});

				// Clear any content ready for next use
				Error = SeriesName = Equation = null;

				// Close the add series wizard
				Parent.Close();

				// Let the concerned parties know that the series pool has changed
				_eventAggregator.PublishOnUIThread(new SeriesPoolChangedEvent());
			} catch (Exception ex) {
				Error = ex.Message;
			}
		}
	}
}
