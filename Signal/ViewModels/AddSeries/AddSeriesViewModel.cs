﻿using Caliburn.Micro;
using Signal.Ui.ViewModels.Wizard;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Signal.Ui.ViewModels.AddSeries {
	public class AddSeriesViewModel : PropertyChangedBase, IWizard {
		private readonly SelectSeriesTypeViewModel _addSeriesSelectSeriesTypeVm;

		public AddSeriesViewModel(SelectSeriesTypeViewModel addSeriesSelectSeriesTypeVm) {
			_addSeriesSelectSeriesTypeVm = addSeriesSelectSeriesTypeVm;

			// Start with the AddSeriesSelectSeriesTypeViewModel page
			_addSeriesSelectSeriesTypeVm.Activate(this);
		}

		private IWizardPage _CurrentPage;
		public IWizardPage CurrentPage {
			get { return _CurrentPage; }
			set {
				if (_CurrentPage != value) {
					_CurrentPage = value;
					NotifyOfPropertyChange(() => CurrentPage);
				}
			}
		}

		public void Close() {
			Complete?.Invoke(this, null);

			// Reset to the start page for next time
			_addSeriesSelectSeriesTypeVm.Activate(this);
		}

		public event EventHandler Complete;
	}
}
