﻿using Caliburn.Micro;
using Signal.Ui.ViewModels.Wizard;
using Signal.Ui.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Signal.Core;
using Signal.Ui.Services;
using System.Windows;
using System.Diagnostics.Contracts;

namespace Signal.Ui.ViewModels.AddSeries {
	public class CsvSelectTimeFieldViewModel : WizardPageBase {
		private readonly CsvSelectValueFieldsViewModel _addSeriesCsvSelectValueFieldsVm;
		private readonly ICsvImporter _csvImporter;
		private readonly IDialogService _dialogService;

		private string _path = null;

		public CsvSelectTimeFieldViewModel(CsvSelectValueFieldsViewModel addSeriesCsvSelectValueFieldsVm, ICsvImporter csvImporter, IDialogService dialogService) {
			_addSeriesCsvSelectValueFieldsVm = addSeriesCsvSelectValueFieldsVm;
			_csvImporter = csvImporter;
			_dialogService = dialogService;

			// Re-evaluate whether we can advance if any of the child properties change
			Fields.AddChildPropertyChangedHandler((s,e) => 
				NotifyOfPropertyChange(() => CanAdvance)
			);
		}
		
		/// <summary>
		/// Load a list of fields from the CSV that may contain the time data
		/// </summary>
		/// <param name="path">The path to the CSV file</param>
		public void LoadPossibleTimeFields(string path) {
			// Save the path to the CSV for when we actually import the data
			_path = path;

			// Clear any existing fields
			Fields.Clear();

			// Create a new CsvFieldViewModel for each field in the CSV file and add it to Fields
			try {
				foreach (var field in _csvImporter.ReadFields(path)) {
					Fields.Add(
						new CsvFieldViewModel() {
							FieldName = field
						}
					);
				}
			} catch (Exception ex) {
				_dialogService.ShowMessageBox("An error occurred whilst importing the CSV data: " + ex.Message, "Error Reading CSV", MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

		public ObservableCollection<CsvFieldViewModel> Fields { get; private set; } = new ObservableCollection<CsvFieldViewModel>();

		private CsvFieldViewModel _SelectedField;
		public CsvFieldViewModel SelectedField {
			get { return _SelectedField; }
			set {
				if (_SelectedField != value) {
					_SelectedField = value;
					NotifyOfPropertyChange(() => SelectedField);
				}
			}
		}

		#region Advance Command

		public bool CanAdvance {
			get {
				// Exactly one field is selected
				return Fields.Count(s => s.IsSelected) == 1;
			}
		}

		public void Advance() {
			Contract.Assert(!string.IsNullOrEmpty(_path), "LoadPossibleTimeFields must be called first");

			_addSeriesCsvSelectValueFieldsVm.Fields.Clear();

			// Add all of the fields except for the one we have just selected to represent time
			_addSeriesCsvSelectValueFieldsVm
				.Fields
				.AddRange(
					Fields
						.Where(s => !s.IsSelected)
						.AsEnumerable()
				);

			_addSeriesCsvSelectValueFieldsVm.LoadSelectableValueFields(_path, SelectedField.FieldName);

			_addSeriesCsvSelectValueFieldsVm.Activate(Parent);
		}

		#endregion
	}
}
