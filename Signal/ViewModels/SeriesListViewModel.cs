﻿using Caliburn.Micro;
using SciChart.Charting.Model.DataSeries;
using Signal.Ui.Events;
using Signal.Ui.ViewModels.AddSeries;
using Signal.Ui.Views;
using Signal.Ui.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel;
using Signal.Core.Timeseries;
using AutoMapper;

namespace Signal.Ui.ViewModels {
	public class SeriesListViewModel : PropertyChangedBase, IHandle<SeriesPoolChangedEvent> {
		private readonly AddSeriesViewModel _addSeriesViewModel;
		private readonly IEventAggregator _eventAggregator;
		private readonly ISeriesPool _seriesPool;

		public SeriesListViewModel(AddSeriesViewModel addSeriesViewModel, IEventAggregator eventAggregator, ISeriesPool seriesPool) {
			_addSeriesViewModel = addSeriesViewModel;
			_eventAggregator = eventAggregator;
			_seriesPool = seriesPool;

			// Hide the AddSeriesViewModel once the wizard is complete
			_addSeriesViewModel.Complete += (s,e) => AddSeriesViewModelVisibility = Visibility.Collapsed;

			// Subscribe to our events
			_eventAggregator.Subscribe(this);

			// Add an event handler for changes to any of the children's properties
			AllSeries.AddChildPropertyChangedHandler(SeriesViewModelChanged);

			// Refresh the chart as a series has been added/removed
			AllSeries.CollectionChanged += (s, e) => RefreshChart();
		}

		private void SeriesViewModelChanged(object sender, PropertyChangedEventArgs args) {
			if (args.PropertyName == "IsDisplayed") {
				RefreshChart();
			}
		}

		#region Add Series Command

		public void AddSeries() {
			AddSeriesViewModelVisibility = Visibility.Visible;
		}

		#endregion

		private Visibility _AddSeriesViewModelVisibility = Visibility.Collapsed;
		public Visibility AddSeriesViewModelVisibility {
			get { return _AddSeriesViewModelVisibility; }
			set {
				if (_AddSeriesViewModelVisibility != value) {
					_AddSeriesViewModelVisibility = value;
					NotifyOfPropertyChange(() => AddSeriesViewModelVisibility);
					NotifyOfPropertyChange(() => AddSeriesButtonVisibility);
				}
			}
		}

		public Visibility AddSeriesButtonVisibility {
			get {
				if (AddSeriesViewModelVisibility == Visibility.Visible) return Visibility.Collapsed;
				else return Visibility.Visible;
			}
		}


		public AddSeriesViewModel AddSeriesViewModel {
			get {
				return _addSeriesViewModel;
			}
		}

		public ObservableCollection<SeriesViewModel> AllSeries { get; private set; } = new ObservableCollection<SeriesViewModel>();

		#region IHandlers

		public void Handle(SeriesPoolChangedEvent seriesLoadedEvent) {
			foreach(var series in _seriesPool.AllSeries) {
				if(!AllSeries.Any(s=>s.Name == series.Name)) {
					// We don't have a view-model for this series, create one
					var seriesVm = Mapper.Map<SeriesViewModel>(series);

					// Display new series by default
					seriesVm.IsDisplayed = true;

					AllSeries.Add(seriesVm);
				}
			}
		}

		#endregion

		public System.Action RefreshChart { get; set; }

		public System.Action ClearChart { get; set; }
	}
}
