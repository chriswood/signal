﻿using Signal.Core.Timeseries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Ui.ViewModels {
	public interface ISeriesDisplay {
		/// <summary>
		/// Add a series to the display. This will only actually be shown if the IsDisplayed property of the SeriesViewModel is true.
		/// </summary>
		/// <param name="series"></param>
		void AddSeries(SeriesViewModel series);

		/// <summary>
		/// Remove a series from the display.
		/// </summary>
		/// <param name="series"></param>
		void RemoveSeries(SeriesViewModel series);

		IEnumerable<SeriesViewModel> Series { get; }
	}
}
