﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Ui.ViewModels.Wizard {
	public class WizardPageBase : PropertyChangedBase, IWizardPage {
		#region IWizardPage members

		public virtual void Activate(IWizard parent) {
			PreviousPage = parent.CurrentPage;
			Parent = parent;

			parent.CurrentPage = this;
		}

		#endregion

		protected IWizard Parent { get; private set; }

		protected IWizardPage PreviousPage { get; private set; }
	}
}
