﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Ui.ViewModels.Wizard {
	public interface IWizardPage {
		void Activate(IWizard parent);
	}
}
