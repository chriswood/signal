using System;
using System.Collections.Generic;
using Caliburn.Micro;
using Ninject;
using Ninject.Modules;
using Signal.Core;
using Signal.Compiler;
using Signal.Ui.ViewModels;
using AutoMapper;
using Signal.Core.Timeseries;

namespace Signal.Ui {
	public class AppBootstrapper : BootstrapperBase {
		StandardKernel Kernel;

		public AppBootstrapper() {
			Initialize();
		}

		protected override void Configure() {
			// Configure Automapper
			Mapper.Initialize(cfg => {
				cfg.CreateMap<Series, SeriesViewModel>();
			});

			// Configure the Ninject kernel
			Kernel = new StandardKernel(
				new INinjectModule[] {
					new CoreNinjectModule(),
					new CompilerNinjectModule(),
					new UiNinjectModule()
				}
			);
		}

		protected override object GetInstance(Type service, string key) {
			return Kernel.Get(service);
		}

		protected override IEnumerable<object> GetAllInstances(Type service) {
			return Kernel.GetAll(service);
		}

		protected override void BuildUp(object instance) {
			Kernel.Inject(instance);
		}

		protected override void OnStartup(object sender, System.Windows.StartupEventArgs e) {
			DisplayRootViewFor<ShellViewModel>();
		}
	}
}