﻿using SciChart.Charting.Model.DataSeries;
using SciChart.Charting.Visuals.RenderableSeries;
using Signal.Ui.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Signal.Ui.Views.SeriesList {
	/// <summary>
	/// Interaction logic for SeriesDisplayView.xaml
	/// </summary>
	public partial class SeriesDisplayView : UserControl {
		public SeriesDisplayView() {
			InitializeComponent();

			DataContextChanged += SeriesDisplayViewDataContextChanged;
		}

		private void SeriesDisplayViewDataContextChanged(object sender, DependencyPropertyChangedEventArgs e) {
			if (ViewModel == null) return;

			ViewModel.RefreshChart = RefreshChart;

			ViewModel.ClearChart = () => sciChartSurface.RenderableSeries.Clear();
		}

		public SeriesListViewModel ViewModel {
			get {
				return DataContext as SeriesListViewModel;
			}
		}

		private void RefreshChart() {
			var displayedSeries = ViewModel.AllSeries.Where(s => s.IsDisplayed);

			// Remove any plots that are no longer set to displayed
			var toRemove = new List<IRenderableSeries>();

			foreach (var renderableSeries in sciChartSurface.RenderableSeries) {
				if (!displayedSeries.Any(s => s.Name == renderableSeries.DataSeries.SeriesName)) {
					toRemove.Add(renderableSeries);
				}
			}

			foreach (var renderableSeries in toRemove) {
				sciChartSurface.RenderableSeries.Remove(renderableSeries);
			}

			// Add any new plots that aren't currently displayed
			foreach (var seriesVm in displayedSeries) {
				if (sciChartSurface.RenderableSeries.Any(s => s.DataSeries.SeriesName == seriesVm.Name)) {
					// This series is already displayed.
					continue;
				}

				XyDataSeries<float, float> dataSeries;

				if (seriesVm.Tag != null) {
					// We have already calculated that chart type for this series. Load the pre-calculated value.
					dataSeries = (XyDataSeries<float, float>)seriesVm.Tag;
				} else {
					// We haven't processed this series before, process it now and store the result on the Tag property of the view-model.
					dataSeries = new XyDataSeries<float, float>(seriesVm.Values.Count);

					dataSeries.AcceptsUnsortedData = true;
					dataSeries.SeriesName = seriesVm.Name;

					foreach (var datapoint in seriesVm.Values) {
						dataSeries.Append(datapoint.Time, datapoint.Value);
					}

					seriesVm.Tag = dataSeries;
				}

				// Add the DataSeries to the RenderableSeries collection so that it is actually displayed
				sciChartSurface.RenderableSeries.Add(
					new FastLineRenderableSeries() {
						DataSeries = dataSeries,
						Stroke = seriesVm.Color
					}
				);
			}
		}
	}
}
