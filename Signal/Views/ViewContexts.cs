﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Ui.Views {
	public static class ViewContexts {
		public const string SeriesSelector = "SeriesSelectorView";
		public const string SeriesDisplay = "SeriesDisplayView";
	}
}
