﻿using Caliburn.Micro;
using Ninject.Modules;
using Signal.Ui.Services;
using Signal.Ui.ViewModels;
using Signal.Ui.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Ui {
	public class UiNinjectModule : NinjectModule {
		public override void Load() {
			// Required binding for Caliburn
			Bind<IWindowManager>().To<WindowManager>().InSingletonScope();
			Bind<IEventAggregator>().To<EventAggregator>().InSingletonScope();

			Bind<IDialogService>().To<DialogService>();

			// We only want one instance of the SeriesListViewModel
			Bind<SeriesListViewModel>().ToSelf().InSingletonScope();
		}
	}
}
