﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Ui.Extensions {
	public static class ObservableCollectionExtensions {
		public static void AddRange<T>(this ObservableCollection<T> collection, IEnumerable<T> items) {
			foreach(var item in items) {
				collection.Add(item);
			}			
		}

		/// <summary>
		/// Adds a PropertyChangedEventHandler to new children as they are added and removes it from them when they are removed. 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="collection"></param>
		/// <param name="handler"></param>
		public static void AddChildPropertyChangedHandler<T>(this ObservableCollection<T> collection, PropertyChangedEventHandler handler) {
			collection.CollectionChanged += (s, e) => {
				// Add an event handler so that we can update our properties when the children change
				if (e.NewItems != null) {
					foreach (PropertyChangedBase vm in e.NewItems) {
						vm.PropertyChanged += handler;
					}
				}

				// Remove any old event handlers to prevent a memory leak
				if (e.OldItems != null) {
					foreach (PropertyChangedBase vm in e.OldItems) {
						vm.PropertyChanged -= handler;
					}
				}
			};
		}
	}
}
