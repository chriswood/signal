﻿using System.Collections.Generic;
using Signal.Core.Timeseries;
using System.Collections.ObjectModel;

namespace Signal.Core {
	public interface ICsvImporter {
		IEnumerable<string> ReadFields(string path);
		Dictionary<string, ReadOnlyCollection<Datapoint>> ReadData(string path, string timeField, IEnumerable<string> dataFields);
	}
}