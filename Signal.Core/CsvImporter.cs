﻿using CsvHelper;
using Signal.Core.Timeseries;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Core {
	internal class CsvImporter : ICsvImporter {
		public IEnumerable<string> ReadFields(string path) {
			using (var reader = new CsvReader(new StreamReader(File.OpenRead(path)))) {
				reader.ReadHeader();

				return reader.FieldHeaders;
			}
		}

		public Dictionary<string, ReadOnlyCollection<Datapoint>> ReadData(string path, string timeField, IEnumerable<string> dataFields) {
			var data = new Dictionary<string, List<Datapoint>>();

			// Create a list for each of the series that we will read
			foreach (var field in dataFields) {
				data.Add(field, new List<Datapoint>());
			}

			// Process the CSV one line at a time
			using (var reader = new CsvReader(new StreamReader(File.OpenRead(path)))) {
				while (reader.Read()) {
					// The same time field applies to all of the data fields
					var time = float.Parse(reader[timeField]);

					// Read the data field values
					foreach (var dataField in dataFields) {
						var value = float.Parse(reader[dataField]);

						data[dataField].Add(new Datapoint(time, value));
					}
				}
			}

			// Change the Lists into immutable ReadOnlyCollections
			var result = new Dictionary<string, ReadOnlyCollection<Datapoint>>();

			foreach(var key in data.Keys) {
				result.Add(key, data[key].AsReadOnly());
			}

			return result;
		}
	}
}
