﻿using System;
using System.Collections.Generic;

namespace Signal.Core.Timeseries {
	public interface ISeriesPool {
		IEnumerable<Series> AllSeries { get; }

		void AddSeries(Series series);
	}
}