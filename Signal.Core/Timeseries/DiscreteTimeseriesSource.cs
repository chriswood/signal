﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Core.Timeseries {
	public class DiscreteTimeseriesSource : ITimeseriesSource {
		private readonly IReadOnlyCollection<Datapoint> _datapoints;

		public DiscreteTimeseriesSource(IEnumerable<Datapoint> datapoints) {
			_datapoints = datapoints
				.OrderBy(s => s.Time)
				.ToList()
				.AsReadOnly();
		}

		public string Name {
			get;
			set;
		}

		public float ValueAtTime(float time) {
			throw new NotImplementedException();
		}
	}
}
