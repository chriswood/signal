﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Core.Timeseries {
	public class Datapoint {
		public Datapoint(float time, float value) {
			Time = time;
			Value = value;
		}

		public float Time { get; private set; }

		public float Value { get; private set; }
	}
}
