﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Core.Timeseries {
	public class Series {
		private static int _instanceNumber = 0;

		public Series() {
			Parameter = (char)('a' + _instanceNumber++);
		}

		public string Name { get; set; }

		public char Parameter { get; set; }

		public ReadOnlyCollection<Datapoint> Values { get; set; }
	}
}
