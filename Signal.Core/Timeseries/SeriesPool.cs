﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Core.Timeseries {
	internal class SeriesPool : ISeriesPool {
		private List<Series> _series = new List<Series>();

		public IEnumerable<Series> AllSeries {
			get {
				return _series;
			}
		}

		public void AddSeries(Series series) {
			_series.Add(series);
		}
	}
}
