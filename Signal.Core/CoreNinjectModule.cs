﻿using Ninject.Modules;
using Signal.Core.Timeseries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Signal.Core {
	public class CoreNinjectModule : NinjectModule {
		public override void Load() {
			Bind<ICsvImporter>().To<CsvImporter>();
			Bind<ISeriesPool>().To<SeriesPool>().InSingletonScope();
		}
	}
}
