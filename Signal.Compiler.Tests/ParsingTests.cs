﻿using Signal.Compiler.Parsing;
using Signal.Compiler.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Signal.Compiler.Tests {
	public class ParsingTests {
		private Delegate Compile(Expression expression, params ParameterExpression[] parameters ) {
			return Expression.Lambda(expression, parameters).Compile();
		}

		[Fact]
		public void TokensMustBeSuppliedToParseMethod() {
			// Null token IEnumerable
			Assert.Throws<ArgumentException>(() => new Parser().Parse(null));

			// Zero tokens
			Assert.Throws<ArgumentException>(() => new Parser().Parse(new Token[] { }));
		}

		[Fact]
		public void VariablesAreAddedToScope() {
			var parser = new Parser();

			var tokens = new Token[] {
				new SeriesToken(0, "a")
			};

			// Parse should complete successfully
			parser.Parse(tokens);

			// The CurrentScope should now contain a variable called 'a'
			Assert.True(parser.Variables.Any(s => s.Name == "a"));
		}

		[Fact]
		public void MoreThanOneOperationCanBeChainedInASingleScope() {
			var parser = new Parser();

			// 3+2*2.3
			var tokens = new Token[] {
				new ConstantToken(0, 3),
				new AdditionToken(1),
				new ConstantToken(2,2),
				new MultiplicationToken(3),
				new ConstantToken(4, 2.3f)
			};

			// First parse should complete successfully
			parser.Parse(tokens);
		}

		[Fact]
		public void NestedExpressionsCanBeUsed() {
			var parser = new Parser();

			// 3+(2*2.3)
			var tokens = new Token[] {
				new ConstantToken(0, 3),
				new AdditionToken(1),
				new OpenBraceToken(2),
					new ConstantToken(2,2),
					new MultiplicationToken(3),
					new ConstantToken(4, 2.3f),
				new CloseBraceToken(5)
			};

			// Parse should complete successfully
			var expression = parser.Parse(tokens);

			// Result of evaluation should be 7.6f
			Assert.Equal(7.6f, Expression.Lambda(expression).Compile().DynamicInvoke());
		}

		[Fact]
		public void DoublyNestedExpressionsCanBeUsed() {
			var parser = new Parser();

			// 3*((2*2.3)+1)
			var tokens = new Token[] {
				new ConstantToken(0, 3),
				new MultiplicationToken(1),
				new OpenBraceToken(2),
					new OpenBraceToken(3),
						new ConstantToken(4,2),
						new MultiplicationToken(5),
						new ConstantToken(6, 2.3f),
					new CloseBraceToken(7),
					new AdditionToken(8),
					new ConstantToken(9, 1),
				new CloseBraceToken(10)
			};

			// Parse should complete successfully
			var expression = parser.Parse(tokens);

			// Result of evaluation should be 16.8f
			Assert.Equal(16.8f, Compile(expression).DynamicInvoke());
		}

		[Fact]
		public void ConstantFollowedByOpenBraceImpliesMultiplication() {
			var parser = new Parser();

			// 3(2+2)
			var tokens = new Token[] {
				new ConstantToken(0, 3),
				new OpenBraceToken(2),
					new ConstantToken(2,2),
					new MultiplicationToken(3),
					new ConstantToken(4, 2),
				new CloseBraceToken(5)
			};

			// Parse should complete successfully
			var expression = parser.Parse(tokens);

			// Result of evaluation should be 12
			Assert.Equal(12f, Compile(expression).DynamicInvoke());
		}

		[Fact]
		public void ConstantFollowedByVariableImpliesMultiplication() {
			var parser = new Parser();

			// 3a
			var tokens = new Token[] {
				new ConstantToken(0, 3),
				new SeriesToken(1, "a")
			};

			// Parse should complete successfully
			var expression = parser.Parse(tokens);

			// Result of evaluation with a=4 should be 12
			Assert.Equal(12f, Compile(expression, parser.Variables.ToArray()).DynamicInvoke(4f));
		}

		[Fact]
		public void ConstantFollowedByVariableSupportAsRhsOfOperation() {
			var parser = new Parser();

			// 3+2a
			var tokens = new Token[] {
				new ConstantToken(0, 3),
				new AdditionToken(1),
				new ConstantToken(2, 2),
				new SeriesToken(3, "a")
			};

			// Parse should complete successfully
			var expression = parser.Parse(tokens);

			// Result of evaluation with a=4 should be 20 due to left to right evaluation
			Assert.Equal(20f, Compile(expression, parser.Variables.ToArray()).DynamicInvoke(4f));
		}

		[Fact]
		public void SameParameterCanBeUsedInDifferentExpressions() {
			var parser = new Parser();

			// a*a
			var tokens = new Token[] {
				new SeriesToken(0, "a"),
				new MultiplicationToken(1),
				new SeriesToken(2, "a"),
			};

			// Parse should complete successfully
			var expression = parser.Parse(tokens);

			// Result of evaluation with a=4 should be 16
			Assert.Equal(16f, Compile(expression, parser.Variables.ToArray()).DynamicInvoke(4f));
		}

	}
}
