﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Signal.Compiler.Lexing;
using Signal.Compiler.Tokens;

namespace Signal.Compiler.Tests {
	public class LexingTests {
		[Fact]
		public void LexerProcessesAllTokensInAddOfTwoConstants() {
			var lexer = new Lexer(new LexerStateMachine());

			var tokens = lexer.Tokenize("1+2");

			Assert.Equal(3, tokens.Count());
		}

		[Fact]
		public void ConstantCanBeFollowedByVariableWithoutOperationBetween() {
			var lexer = new Lexer(new LexerStateMachine());

			var tokens = lexer.Tokenize("2.3ab");

			Assert.Equal(2, tokens.Count());
			Assert.IsType<ConstantToken>(tokens.ElementAt(0));
			Assert.IsType<SeriesToken>(tokens.ElementAt(1));
		}
	}
}
